// Fonction pour rendre les valeurs des champs plus user-friendly
function stringBeautify (input) {
  if (input === undefined) {
    return '';
  }
  if (typeof (input) !== 'string') {
    return input;
  }
  if (!input.startsWith('http')) {
    let res = input[0].toUpperCase() + input.slice(1);
    res = res.replace('_', ' ');
    return res;
  }
  return input;
}
