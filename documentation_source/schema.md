# Un schéma JSON pour les Chatons

Le schéma le plus à jour est [disponible ici](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/schema/CHATONS_schema-0.0.2.json) (v0.0.2).
## Le JSON
Dans ce projet Tx, chaque Chaton est décrit par un fichier JSON (voir par exemple celui de [Picasoft](https://picasoft.net/picasoft.JSON)). Un fichier JSON décrit une structure de listes et d'objets définis par un système de clé-valeur. Par exemple, un Chaton a un champ `name` qui prend la valeur du nom du Chaton, un champ `url` qui prend l'URL du site du Chaton, etc. Mais un Chaton propose également une liste de services, eux aussi décris par ce formalisme, avec un champ `type`, un champ `description`...

Afin de permettre à un outil de recherche de rechercher des informations dans des champs précis, il est nécessaire de standardiser les clés de ces champs : si `name`, `nom` ou `nom_chaton` représentent la même information, il faut que tous les chatons respectent les mêmes conventions.

La convention que nous avons choisie en début de projet [a ses faiblesses](formatage_donnees.md). Il conviendrait de refonder le schéma, avec des valeurs soit en anglais, soit en «&nbsp;vrai&nbsp;» français.

## Le schéma
Pour ce faire, nous avons créé un *schéma*, lui aussi exprimé en langage JSON, qui explique quel champ est à utiliser pour quoi, et qui permet de *valider* un JSON décrivant un Chaton, garantissant que l'information est correctement formatée et sera utilisable par l'outil de recheche.

La version actuelle du schéma est la [0.0.2](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/schema/CHATONS_schema-0.0.2.json). Cette information est précisée dans le champ `$id`. Ce champ permet de renseigner l'identifiant unique du schéma, avec notamment sa version, afin d'avoir des données plus cohérentes. C'est une pratique mentionnée dans le [guide officiel des schémas JSON](https://json-schema.org/understanding-json-schema/basics.html#declaring-a-unique-identifier).

[Notre schéma](http://chatons.picasoft.net/schema/schema.json) indique par exemple qu'un fichier JSON décrivant un Chaton est valide s'il a un champ `name`, `url`, `status` et `structure`, que s'il propose des services alors ces derniers ont un champ `type` et éventuellement une `description`...

Pour commencer à décrire votre Chaton, un [template](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/schema/exemples/template.json) vide est disponible sur le Framagit, ainsi que quelques [exemples](https://framagit.org/bertille/tx-collecte-chatons/-/tree/master/schema/exemples).

Il est demandé aux Chatons de ne fournir que des JSON valides selon ce schéma, et encodés en utf-8, afin de permettre à l'outil de recherche de fonctionner. Les fichiers non-valides ne seraient simplement pas ajoutés à la liste des Chatons utilisée pour la recherche par le [crawler](http://chatons.picasoft.net/documentation/crawler/). Si d'aventure vous avez le moindre retour à faire sur ce schéma, n'hésitez pas à ouvrir une issue sur le [Framagit](https://framagit.org/bertille/tx-collecte-chatons/), ou à nous contacter sur le [forum](https://forum.chatons.org/t/conception-dun-outil-de-centralisation-des-informations-des-chatons/1074) du CHATONS.
