# Un convertisseur JSON-LD ?
Le JSON-LD est un formalisme du web sémantique. Il permet de donner du sens aux données en standardisant les noms des champs et en décrivant des types d'objets. [Cette vidéo de présentation permet de comprendre les enjeux du JSON-LD](https://www.youtube.com/watch?v=vioCbTo3C-4).

C'est une possibilité intéressante pour les Chatons, qu'on pourrait décrire comme des [organisations](https://schema.org/Organization). Ainsi, ils seraient plus facilement référencés, et comparables à d'autres organisations, en particulier à d'autres fournisseurs de services associatifs dans d'autres pays.

Cependant, pour donner des informations à la fois sur un Chaton et ses services, il faudrait mobiliser plusieurs schémas de JSON-LD dans le [contexte](https://www.w3.org/TR/json-ld/#the-context), et donc écrire un convertisseur un peu plus complexe ou bien définir notre propre document de contexte. C'est un chantier dans lequel nous n'avons pas eu le temps de nous lancer.

Pour un convertisseur simple, la correspondance des champs entre une Organization et notre schéma 0.0.2 serait :
```json
{  
    "@context" : "http://schema.org",  
    "@type" : "Organization",  
    "name" : name,  
    "url": url,  
    "description": description,  
    "address" : address,  
    "logo": logo,  
    "areaServed": perimeter,  
    "foundingDate": creation_date  
}  
```  
